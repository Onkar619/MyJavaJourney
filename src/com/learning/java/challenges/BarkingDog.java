package com.learning.java.challenges;

public class BarkingDog {
    private boolean isBarking;
    private int hourOfDay;

    public BarkingDog(){

    }

    public BarkingDog(boolean isBarking, int hourOfDay) {
        this.isBarking = isBarking;
        this.hourOfDay = hourOfDay;
    }

    public void setBarking(boolean barking) {
        isBarking = barking;
    }

    public void setHourOfDay(int hourOfDay) {
        this.hourOfDay = hourOfDay;
    }

    public boolean isBarking() {
        return isBarking;
    }

    public int getHourOfDay() {
        return hourOfDay;
    }
    public boolean shouldWakeUp(){

        if(((this.hourOfDay>22 && this.hourOfDay<=23) ||(this.hourOfDay>0 && this.hourOfDay<8)) && this.isBarking==true){
           return true;

        }
        else return false;
    }

}
