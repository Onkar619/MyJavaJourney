package com.learning.java.challenges;

public class LeapYear {

    private int year;


    public LeapYear(int year){
        this.year = year;
    }
    public boolean isLeapYear(){

        if(!(this.year<=0) && !(this.year>9999)){
            if((this.year%400 == 0) || (this.year%4==0 && (this.year%100)!=0)){
                return true;
            }

        }
        return false;

    }



}
