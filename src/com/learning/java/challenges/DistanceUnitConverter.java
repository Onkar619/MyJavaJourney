package com.learning.java.challenges;

public class DistanceUnitConverter {

    private final double conversionConstant = 1.609344;



    public long ToMilesPerHour(double kilometerPerHour){

        if(!(kilometerPerHour<0)){
            long speedInMiles = (long) (kilometerPerHour / conversionConstant);
            return Math.round(speedInMiles);
        }else{
            return -1;
        }

    }

}
