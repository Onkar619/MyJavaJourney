package com.learning.java.methodoverloading;

public class Area {


    private double radius;
    private double length;
    private double breadth;
    static final double pi = 3.14;


    public Area(){

    }

    public Area(double radius, double length, double breadth) {
        this.radius = radius;
        this.length = length;
        this.breadth = breadth;
    }

    public double TotalArea(double radius){
        if(radius<=0){
            return -1;
        }
        else{
            double result = pi*radius*radius;
            return result;
        }

    }

    public double TotalArea(double length,double breadth){

        if(length<=0 || breadth<=0){
            return -1;
        }
        else{
            double result = length*breadth;
            return result;
        }

    }

}
