package com.learning.java;

import com.learning.java.challenges.BarkingDog;
import com.learning.java.challenges.DistanceUnitConverter;
import com.learning.java.challenges.LeapYear;
import com.learning.java.methodoverloading.Area;

public class Main {

    public static void main(String[] args) {
	// write your code here

        DistanceUnitConverter convert = new DistanceUnitConverter();
        long result = convert.ToMilesPerHour(25.42);
        System.out.println("To Miles = " + result);

        BarkingDog dog= new BarkingDog(true,-1);
        boolean result2 = dog.shouldWakeUp();
        System.out.println("should wake up? = " + result2);

        LeapYear year = new LeapYear(1600);
        boolean result3 = year.isLeapYear();
        System.out.println("is leap year? = " + result3);

        Area area = new Area();
        double areaCircle = area.TotalArea(15.55);
        double areaRectangle = area.TotalArea(15.5,34.5);
        System.out.println("circle area = " + areaCircle);
        System.out.println("circle rectangle = " + areaRectangle);
    }
}
